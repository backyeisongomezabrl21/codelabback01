package com.techu.backuno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackunoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackunoApplication.class, args);
	}

}
