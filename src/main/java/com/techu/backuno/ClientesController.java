package com.techu.backuno;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ClientesController {

    private ArrayList<Cliente> listaClientes = new ArrayList<>();
    public CuentasController cuenta = new CuentasController();


    /*Get obtener cliente por id*/
    @GetMapping(value = "/clientes/{id}")
    public ResponseEntity<Cliente> getClienteId(@PathVariable int id){

        for (Cliente cliente: listaClientes) {
            if (cliente.getId() == id) {
                return new ResponseEntity<>(cliente, HttpStatus.OK);
            }
        }
        return new ResponseEntity("El cliente no se encuentra registrado...", HttpStatus.NOT_FOUND);
    }

    /*Get para obtener la lista de cliente*/
    @GetMapping(value = "/clientes")
    public ResponseEntity<Cliente> getClientes(){
        return new ResponseEntity(listaClientes, HttpStatus.NOT_FOUND);
    }

    /*Post para agregar un nuevo cliente*/
    @PostMapping(value = "/clientes")
    public ResponseEntity<String> addCliente(@RequestBody Cliente newCliente){
        for (Cliente cliente: listaClientes) {
            if (cliente.getId() == newCliente.getId()) {
                return new ResponseEntity<>("Ya hay un cliente registrado con el id."+ cliente.getId(), HttpStatus.CREATED);
            }
        }
        listaClientes.add(newCliente);

        cuenta.crearCuenta(newCliente.getId());

        return new ResponseEntity<>("Cliente creado correctamente", HttpStatus.CREATED);
    }

    /*Put para modificar un cliente*/
    @PutMapping(value = "/clientes/{id}")
    public ResponseEntity<String> updateCliente(@PathVariable int id, @RequestBody Cliente cambioCliente){
        int indice = 0;
        for (Cliente cliente: listaClientes) {
            if(cliente.getId() == id) {
                cliente.setNombre(cambioCliente.getNombre());
                cliente.setDireccion(cambioCliente.getDireccion());
                cliente.setTelefono((cambioCliente.getTelefono()));
                cliente.setActivo(cambioCliente.isActivo());
                listaClientes.set(indice, cliente);
               return new ResponseEntity<>("Cliente actualizado correctamente", HttpStatus.OK);
            }
            indice++;
        }
        return new ResponseEntity("El cliente no se encuentra registrado...", HttpStatus.NOT_FOUND);
    }

    /*Delete cliente por id*/
    @DeleteMapping(value = "/clientes/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable int id){
        int indice = 0;
        for (Cliente cliente: listaClientes) {
            if (cliente.getId() == id) {
                listaClientes.remove(indice);
               return new ResponseEntity<>("Cliente eliminado correctamente...", HttpStatus.NO_CONTENT);
            }
            indice++;
        }
        return new ResponseEntity<>("El cliente no se encuentra registrado...", HttpStatus.NOT_FOUND);
    }

    /*Patch para activar o bloquear al cliente*/
    @PatchMapping(value = "/clientes/{id}")
    public ResponseEntity<String> updateEstadoCliente(@PathVariable int id, @RequestBody Cliente cambioEstado){
        int indice = 0;
        for (Cliente cliente: listaClientes) {
            if(cliente.getId() == id) {
                cliente.setActivo(cambioEstado.isActivo());
                listaClientes.set(indice, cliente);
                return new ResponseEntity<>("Cliente actualizado correctamente", HttpStatus.OK);
            }
            indice++;
        }
        return new ResponseEntity("El cliente no se encuentra registrado...", HttpStatus.NOT_FOUND);
    }

    /*Get sub recurso para consultar la cuenta de un cliente*/
    @GetMapping(value = "/clientes/cuentas/{id}")
    public ResponseEntity<List<Cuenta>> getCuentaCliente(@PathVariable int id){
        for (Cliente cliente: listaClientes) {
            if (cliente.getId() == id) {
                return new ResponseEntity(cuenta.consultarCuenta(id), HttpStatus.OK);
            }
        }
        return new ResponseEntity("El cliente no se encuentra registrado...", HttpStatus.NOT_FOUND);
    }

}
