package com.techu.backuno;

public class Cuenta {
    private long numeroCuenta;
    private  int idCliente;

    public Cuenta(long numeroCuenta, int idCliente) {
        this.numeroCuenta = numeroCuenta;
        this.idCliente = idCliente;
    }

    public long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(long numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}
