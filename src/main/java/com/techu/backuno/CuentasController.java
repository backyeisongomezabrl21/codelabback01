package com.techu.backuno;

import java.util.ArrayList;
import java.util.Random;

public class CuentasController {

    public ArrayList<Cuenta> listaCuenta = new ArrayList<>();

    /* Metodo para crear cuenta relacionada a un cliente*/
    public void crearCuenta(int idCliente){
        long cuenta = Math.abs(new Random(idCliente).nextLong());
        listaCuenta.add(new Cuenta(cuenta, idCliente));
        System.out.println("Se ha creado la cuenta: "+ String.valueOf(cuenta) + " Al cliente: "+ String.valueOf(idCliente));
    }

    /* Metodo para consultar cuenta relacionada a un cliente*/
    public Cuenta consultarCuenta(int idCliente){
        for (Cuenta cuenta: listaCuenta) {
            if (cuenta.getIdCliente() == idCliente) {
                System.out.println("Se ha encontrado la cuenta: "+ String.valueOf(cuenta.getNumeroCuenta()));
                return cuenta;
            }
        }
        return null;
    }


}
